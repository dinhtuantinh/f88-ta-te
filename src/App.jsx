import React from 'react';
import './App.scss';
import Menu from './page/home/menu';

function App() {
  return (
    <div className="App">
      <Menu />
    </div>
  );
}

export default App;
