import '../../layout/home/menu.scss';

function Menu() {
    return (
        <div className="Menu">
            <div className="top_menu">
                <ul className="top_menu_ul">
                    <li>
                        <a href="/#">Về F88</a>
                    </li>
                    <li>
                        <a href="/#">Quan hệ Nhà đầu tư</a>
                    </li>
                    <li>
                        <a href="/#">Tin tức</a>
                    </li>
                    <li>
                        <a href="/#">Cửa hàng gần bạn</a>
                    </li>
                    <li className='li_end'>
                        <a href="/#">Kiến thức tài chính</a>
                    </li>
                </ul>
            </div>
            <div className="down_menu">
                <div className="contener">
                    <div className="logo">
                        <img src="https://f88.vn/img/footer/logo-f88.svg" alt="" />
                    </div>
                    <div className="dropdown">
                        <div className='option'>
                            Cho vay cầm cố
                            <img src="https://f88.vn/img/iconSelect.svg" alt="" />
                        </div>
                        <div className='option'>
                            Cho vay cầm cố
                            <img src="https://f88.vn/img/iconSelect.svg" alt="" />
                        </div>
                        <div className='option'>
                            Cho vay cầm cố
                            <img src="https://f88.vn/img/iconSelect.svg" alt="" />
                        </div>
                        <div className='option'>
                            Cho vay cầm cố
                            <img src="https://f88.vn/img/iconSelect.svg" alt="" />
                        </div>
                        <div className='option'>
                            Cho vay cầm cố
                            <img src="https://f88.vn/img/iconSelect.svg" alt="" />
                        </div>
                    </div>
                    <div className="hotline">
                        <a href="/#"><img src="https://f88.vn/img/iconPhoneHeader.svg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Menu;
